# Wanderu Deutschebahn

## Requirements

Write a Node JS program (Node 8+) that can find and output the
5 center-most stations in Berlin and 5 center-most in Hamburg. Center-most
means closest to the known latitude/longitude center of the given city.
From those stations, find and output 5 station pairs from Hamburg to
Berlin and their travel distances with the following criteria: The Hamburg
station has to have parking. The Berlin station has to have public
transportation available.

### Running Application

- Make sure node and npm are installed (developed with v10.8.0)
- Place "config.json" file in root directory (format below)
- npm install
- node index.js

#### config.json
```json
{
  "deutschebahn": {
    "url": "https://api.deutschebahn.com/stada/v2",
    "key": "deutschebahn api key"
  }
}
```

