'use strict';

const _ = require('lodash/fp');
const Distance = require('./distance.js');
const Logger = require('./logger.js');

/**
 * Parse stations returned from Deutschebahn.
 * stations - Deutschebahn stations.
*/
exports.parseStations = function(stations, city) {
  Logger.info('Attempting to parse stations');
  return _
    .chain(stations)
    .map(x => parseStation(x, city))
    .filter(x => x != undefined)
    .value();
}

/**
 * Parse station returned from Deutschebahn.
 * station - Deutschebahn station.
*/
function parseStation(station, city) {
  try {
    Logger.info('Parsing station object');

    // Parse out eva coords.
    Logger.info('Attempting to get coordinates from station');
    const _coords = _
      .chain(station.evaNumbers)
      .map(x => x.geographicCoordinates)
      .filter(x =>
        x != undefined &&
        x.type.toLowerCase() === "point" &&
        x.coordinates.length === 2)
      .map(x => ({
        lat: x.coordinates[1],
        lng: x.coordinates[0] }))
      .map(x => {
        x.distance = Distance
          .calcDistance(x, city.coordinates);
        return x; })
      .sortBy(x => x.distance)
      .value();

    // If not coords are found return undefined and log warning.
    if(_coords.length === 0) {
      Logger.warn(`Station without coordinates found. STATION: ${station.number}, ${station.name}`);
      Logger.warn(station.evaNumbers);
      return undefined;
    }

    if(_coords.length > 1) {
      Logger.info('Multiple coordinates found');

      for(let i = 0; i < _coords.length; i++) {
        Logger.info(_coords[i]);
      }
    }

    const coords = _coords[0];
    Logger.info(`Station Coordinates: ${coords.lat}, ${coords.lng}`);
    Logger.info(`Station distance from ${city.name}: ${coords.distance}`)

    const res = {
      number: station.number,
      name: station.name,
      coordinates: coords,
      federalState: station.federalState,
      hasParking: station.hasParking,
      hasLocalPublicTransport: station.hasLocalPublicTransport
    }

    return res;
  } catch(err) {
    Logger.error('Failed to parse station');
    Logger.error(err.message);
    return undefined;
  }
}

