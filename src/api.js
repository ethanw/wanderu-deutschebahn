'use strict';

const _ = require('lodash/fp');
const Config = require('../config.json');
const Logger = require('./logger.js');
const Parse = require('./parseStations.js');
const Swagger = require('swagger-client');

/**
 * Make request to Deutschebahn api.
 * stations - Deutschebahn stations.
*/
async function requestStations(city) {
  const request = {
    url: `${Config.deutschebahn.url}/stations${city.queryFilter}`,
    method: 'GET',
    headers: { 'Authorization': `Bearer ${Config.deutschebahn.key}` }
  }

  Logger.info(`Finding stations for ${city.name}`);
  Logger.info(`Sending request to Deutschebahn. URL: ${request.url}`)
  try {
    const res = await Swagger.http(request);
    const code = res.status;
    Logger.info('Response received from Deutschebahn Server');
    Logger.info(`Status Code: ${code}`);
    switch (true) {
      case (code >= 200 && code < 300):
        Logger.info(`${res.body.length} Stations returned`);
        const stations = Parse.parseStations(res.body.result, city);

        return _
          .chain(stations)
          .sortBy(x => x.coordinates.distance)
          .take(5)
          .value()

      case (code >= 400 && code < 500):
        Logger.error(`Deutschebahn Server responded with ${code} status code`);
        Logger.error(res.statusText);
        return [];
      case (code >= 500):
        Logger.error('Deutschebahn Server error');
        Logger.error(res.statusText);
        return[];
    }
  } catch (err) {
    Logger.error('Request to Deutschebahn Server failed');
    Logger.error(err.message);
    return[];
  }
}

module.exports = {
  requestStations: requestStations
};

