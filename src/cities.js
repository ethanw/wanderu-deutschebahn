'use strict';

/**
 * Berlin coordinates and filters.
*/
const berlin = {
  name: 'Berlin',
  coordinates: {
    lat: 52.520008,
    lng: 13.404954
  },
  queryFilter: "?federalstate=berlin"
}

/**
 * Hamburg coordinates and filters.
*/
const hamburg = {
  name: 'Hamburg',
  coordinates: {
    lat: 53.551086,
    lng: 9.993682
  },
  queryFilter: "?federalstate=hamburg"
}

module.exports = {
  berlin: berlin,
  hamburg: hamburg
};

