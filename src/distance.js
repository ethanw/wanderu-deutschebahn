'use strict';

const _ = require('lodash/fp');
const LatLon = require('geodesy').LatLonVectors;

/**
 * Calculate the distance in miles between two points.
 *  coords1 - First  point { lat: ..., lng: ...}
 *  coords2 - Second point { lat: ..., lng: ...}
 */
const distance = {
  calcDistance: function(coords1, coords2) {
    const origin = new LatLon(coords1.lat, coords1.lng);
    const destination = new LatLon(coords2.lat, coords2.lng);
    return origin.distanceTo(destination, 3959).toPrecision(3);
  },

  /**
   * Find closes routes between stations in two cities.
   *  originStations - collection of stations.
   *  destStations - collection of stations.
   *
   * This is likely incorrect since it uses geological distance
   * instead of travel distance.
   */
  findRoutes: function(originStations, destStations) {
    if(originStations.length === 0 || destStations.length === 0) {
      new Error('At least 1 origin station and 1 destination station is required');
    }

    return _
      .chain(originStations)
      .flatMap(orig => _
        .chain(destStations)
        .map(dest => ({
          origNumber: orig.number,
          origName: orig.name,
          destNumber: dest.number,
          destName: dest.name,
          distance: this.calcDistance(
            orig.coordinates, dest.coordinates)}))
        .value())
      .sortBy(x => x.distance)
      .take(5)
      .value()

    return _.map(x => {
      const destinations = _
        .chain(destStations)
        .map(y => ({
          number: y.number,
          name: y.name,
          distance: this.calcDistance(x.coordinates, y.coordinates)}))
        .sortBy(x => x.distance);
      return {
        originNumber: x.number,
        originName: x.name,
        destNumber: y[0].number,
        destName: y[0].name,
        distance: y[0].distance
      }}, originStations)
  }
};

module.exports = distance;

