'use strict';

const _ = require('lodash/fp');
const Api = require('./src/api.js');
const Cities = require('./src/cities.js');
const Distance = require('./src/distance.js')
const Logger = require('./src/logger.js');

function printStation(station) {
  console.log('----------');
  console.log(`Station Number: ${station.number}`);
  console.log(`Station Name: ${station.name}`);
  console.log(`Federal State: ${station.federalState}`);
  console.log(`Coordinates: ${station.coordinates.lat}, ${station.coordinates.lng}`);
  console.log(`Distance (Miles): ${station.coordinates.distance}`);
  console.log(`Public Transportation: ${station.hasLocalPublicTransport}`);
  console.log(`Parking: ${station.hasParking}`);
}

function printRoute(route) {
  console.log('----------');
  console.log(`Origin Station number: ${route.origNumber}`);
  console.log(`Origin Station Name: ${route.origName}`);
  console.log(`Destination Station Number: ${route.destNumber}`);
  console.log(`Destination Station Name: ${route.destName}`);
  console.log(`Distance (Miles): ${route.distance}`);
}

async function main() {
  Logger.info('Get station data');

  const [berlinStations, hamburgStations] = await Promise.all([
    Api.requestStations(Cities.berlin),
    Api.requestStations(Cities.hamburg)]);

  Logger.info('Find routes from Berlin to Hamburg');
  const routes = Distance.findRoutes(
    _.filter(x => x.hasLocalPublicTransport, berlinStations),
    _.filter(x => x.hasParking, hamburgStations));

  console.log('\n\nCenter Most Berlin Stations');
  for(let i = 0; i < berlinStations.length; i++) {
    printStation(berlinStations[i]);
  }

  console.log('\n\nCenter Most Hamburg Stations')
  for(let i = 0; i < hamburgStations.length; i++) {
    printStation(hamburgStations[i]);
  }

  console.log('\n\nRoutes from Berlin to Hamburg')
  for(let i = 0; i < routes.length; i++) {
    printRoute(routes[i]);
  }
}

main();

